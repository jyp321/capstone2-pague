let formSubmit = document.querySelector("#createCourse")

//add an event listener:
formSubmit.addEventListener("submit", (e) => {
	//Q: What does preventDefault() do?
	//A: It prevents the normal behavior of an event. In this case, the event is submit and its default behavior is to refresh the page when submitting the form.
	e.preventDefault()

	//get the values of your input:
	let courseName = document.querySelector("#courseName").value
	let description = document.querySelector("#courseDescription").value
	let price = document.querySelector("#coursePrice").value

	//Common Error:
	//Cannot read property "values" of null
	//Ex:
	//object.name
	//Cannot read property name of null
	//object is null.

	//Get the JWT from our localStorage
	let token = localStorage.getItem("token")
	console.log(token)

	//Create a fetch request to add a new course:
	fetch('https://radiant-stream-95158.herokuapp.com/api/courses', {

		method: 'POST',
		headers: {

			'Content-Type': 'application/json',
			'Authorization': `Bearer ${token}`

		},
		body: JSON.stringify({
			name: courseName,
			description: description,
			price: price
		})

	})
	.then(res => res.json())
	.then(data => {

		//if the creation of the course is successful, redirect admin to the courses page.

		if(data === true){

			//redirect the admin to the courses page.
			window.location.replace('./courses.html')

		}else {

			//Error while creating a course:
			alert("Course Creation Failed. Something Went Wrong.")

		}

	})


})