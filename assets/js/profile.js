let name = document.querySelector("#userName");
let desc = document.querySelector("#userDesc");
let userCourses = document.querySelector("#enrollContainer");
let token = localStorage.getItem('token');
let courseArray = [];
let editButton = document.querySelector('#editButton')

fetch(`https://radiant-stream-95158.herokuapp.com/api/users/details`, {
	headers: {
		'Authorization' : `Bearer ${token}`
	}
})
.then(res => res.json())
.then(data => {
	if(data) {
		name.innerHTML = `${data.firstName} ${data.lastName}`
		desc.innerHTML = `Mobile Number: ${data.mobileNo} Email: ${data.email}`
		editButton.innerHTML = 
		`
			<div id="btnn" class="d-flex align-items-center justify-content-center bg-primary">
				<a class="my-2" href="./editProfile.html?userId=${data._id}" value="{data._id}">Edit Profile</a>
			</div>
		`;
	} else {
		alert("Something went wrong");
	}

	data.enrollments.forEach(course => {
		console.log(course)
	})
	data.enrollments.forEach(course => {
		let courseId = course.courseId
		fetch(`https://radiant-stream-95158.herokuapp.com/api/courses/${courseId}`, {

			headers: {
				"Content-Type": "application/json",
				"Authorization": `Bearer ${token}`
			}
		})
		.then(res => res.json())
		.then(data => {

			if(data) {
				userCourses.innerHTML +=
				`
					<div class="card my-5">
						<div class="card-body">
						<h4 class="card-title">${data.name}</h4>
						<h5 class="card-title">${course.enrolledOn}</h5>
						<p class="card-title">${course.status}</p>
						</div>
					</div>
				`
			} else {
				alert("Something went wrong")
			}
		})
	})
})

