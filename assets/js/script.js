let navItems = document.querySelector("#navSession");
let registerLink = document.querySelector("#register");
let profile = document.querySelector("#profile");
let admin = localStorage.getItem("isAdmin") === "true"

let userToken = localStorage.getItem("token");

if(!userToken) {
	navItems.innerHTML = 
		`
			<li class="nav-item ">
				<a href="./login.html" class="nav-link"> Login </a>
			</li>
		`
	registerLink.innerHTML =
		`
			<li class="nav-item ">
				<a href="./register.html" class="nav-link"> Register </a>
			</li>
		`
} else {


	if(admin == false){
		profile.innerHTML = 
		`
	 		<li class="nav-item">
	 			<a href="./profile.html" class="nav-link"> Profile </a>
	 		</li>
	 	`
	 	navItems.innerHTML = 
		`
			<li class="nav-item">
				<a href="./logout.html" class="nav-link"> Log Out </a>
			</li>
		`
	} else {
		navItems.innerHTML = 
		`
			<li class="nav-item">
				<a href="./logout.html" class="nav-link"> Log Out </a>
			</li>
		`
	}
}