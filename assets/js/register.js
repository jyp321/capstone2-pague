let registerForm = document.querySelector("#registerUser")

//addEventListener("event", function)
/*
	Allows us to add a specific event into an element. This event can trigger a function for our page to do.
*/

//Submit event - allows us to submit a form.
//It's default behavior is that it sends your form and refreshes the page
registerForm.addEventListener("submit", (e) => {

	//e = event object. This event object pertains to the event and where it was triggered.
	//preventDefault() = prevents the submit event of its default behavior

	e.preventDefault()

	console.log("I triggered the submit event")

	let firstName = document.querySelector("#firstName").value
	let lastName = document.querySelector("#lastName").value
	let mobileNo = document.querySelector("#mobileNumber").value
	let email = document.querySelector("#userEmail").value
	let password1 = document.querySelector("#password1").value
	let password2 = document.querySelector("#password2").value


	if((password1 !== '' && password2 !== '' ) && (password1 === password2) && (mobileNo.length === 11)){

		// fetch( url , options )
		fetch('https://radiant-stream-95158.herokuapp.com/api/users/email-exists', {
			method: 'POST',
			headers: { 'Content-Type': 'application/json' },
			body: JSON.stringify({
				email: email
			})
			// "{email: email}"
		})
		.then(res => res.json())
		.then(data => {

			console.log(data)

			if (data === false) {

				fetch('https://radiant-stream-95158.herokuapp.com/api/users', {
					method: 'POST',
					headers: { 'Content-Type': 'application/json' },
					body: JSON.stringify({
						firstName: firstName,
						lastName: lastName,
						email: email,
						password: password1,
						mobileNo: mobileNo
					})
				})
				.then(res => res.json())
				.then(data => {

					console.log(data);

					if(data === true){
						alert("registered successfully")

						//redirect to login page
						window.location.replace("./login.html")
					} else {
						// error in creating registration
						alert("something went wrong")
					}

				})

			}

		})

	}

})