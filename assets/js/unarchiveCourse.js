let params = new URLSearchParams(window.location.search)
let courseId = params.get('courseId')
let token = localStorage.getItem('token')

fetch(`https://radiant-stream-95158.herokuapp.com/api/courses/${courseId}`, {
	method: 'PUT',
	headers: {
		'Content-Type': 'application/json',
		'Authorization' : `Bearer ${token}`
	},
	body: JSON.stringify({
		courseId: courseId
	})
})
.then(res => res.json())
.then(data => {
	if (data) {
		window.location.replace('./courses.html')
	} else {
		alert('Something went wrong')
	}
	console.log(data)
})